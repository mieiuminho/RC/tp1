PROJECT  = article
TEX      = pdflatex
BIBTEX   = bibtex
OPTIONS  = -synctex=1 --interaction=nonstopmode -file-line-error

develop:
	@latexmk -pvc -silent -pdflatex="$(TEX) $(OPTIONS) %O %S" $(PROJECT).tex

pdf:
	latexmk -pdf -pdflatex="$(TEX) $(OPTIONS) %O %S" $(PROJECT).tex
	mv $(PROJECT).pdf TP1.PL2.G07.pdf

build:
	$(TEX) $(PROJECT).tex
	$(BIBTEX) $(PROJECT)
	$(TEX) $(PROJECT).tex
	$(TEX) $(PROJECT).tex

clean:
	@echo "Cleaning..."
	@curl https://raw.githubusercontent.com/nelsonmestevao/spells/master/art/maid.ascii
	@latexmk -C -silent
	@echo "...✓ done!"
